return {
  ["workshop-100010002"]={
    configuration_options={
      amount_of_numbers="default",
      display_mode="waving",
      dmg_only="on",
      show_decimal_points="no" 
    },
    enabled=true 
  },
  ["workshop-100010007"]={
    configuration_options={ fast_eat="On", fast_pick_build_harvest_heal="On" },
    enabled=true 
  },
  ["workshop-100010016"]={ configuration_options={  }, enabled=true },
  ["workshop-100010018"]={ configuration_options={ kong=0, maxdiaoluo=4, mindiaoluo=1 }, enabled=true },
  ["workshop-100010020"]={
    configuration_options={
      RUOYINXIAN=false,
      SHENGDANSHI=false,
      baoshibaolv=1,
      blue_baoshi=1,
      er_shuoming=0,
      green_baoshi=1,
      marble_suipian=1,
      orange_baoshi=1,
      purple_baoshi=1,
      red_baoshi=1,
      shitoubaolv=1,
      thulecite_xiukuang=1,
      wajuecishu=2,
      yellow_baoshi=1,
      yi_shuoming=0 
    },
    enabled=true 
  },
  ["workshop-100010022"]={
    configuration_options={ dont_give_dubloons=2, give_dubloons=1, position=1, rec_dubloon=3, rec_gold=3 },
    enabled=true 
  },
  ["workshop-100010023"]={
    configuration_options={
      FIREOPTIONS=2,
      OVERRIDEMODE=false,
      SHAREMINIMAPPROGRESS=true,
      SHOWFIREICONS=true,
      SHOWPLAYERICONS=true,
      SHOWPLAYERSOPTIONS=2 
    },
    enabled=true 
  },
  ["workshop-100010024"]={
    configuration_options={
      Craft="Normal",
      Destroyable="yees",
      FoodSpoilage=1,
      Language="En",
      Position="Center",
      Slots=80 
    },
    enabled=true 
  },
  ["workshop-100010027"]={ configuration_options={ ["Draw over FoW"]="disabled" }, enabled=true },
  ["workshop-100010033"]={ configuration_options={ speed="0.25" }, enabled=true },
  ["workshop-100010041"]={ configuration_options={ Ownership=false, Travel_Cost=32 }, enabled=true },
  ["workshop-100010059"]={ configuration_options={  }, enabled=true },
  ["workshop-100010062"]={
    configuration_options={
      divider=5,
      random_health_value=0,
      random_range=0,
      send_unknwon_prefabs=true,
      show_type=0,
      unknwon_prefabs=1,
      use_blacklist=true 
    },
    enabled=true 
  },
  ["workshop-100010063"]={ configuration_options={ MAXSTACKSIZE=99 }, enabled=true },
  ["workshop-2199027653598511644"]={ configuration_options={  }, enabled=true },
  ["workshop-2199027653598512364"]={ configuration_options={  }, enabled=true },
  ["workshop-2199027653598516235"]={
    configuration_options={
      RUOYINXIAN=false,
      SHENGDANSHI=false,
      baoshibaolv=1,
      blue_baoshi=1,
      er_shuoming=0,
      green_baoshi=1,
      huangjinbaolv=1,
      marble_suipian=1,
      orange_baoshi=1,
      purple_baoshi=1,
      red_baoshi=1,
      shishengzhang=0,
      shitoubaolv=1,
      thulecite_xiukuang=1,
      wajuecishu=2,
      yellow_baoshi=1,
      yi_shuoming=0 
    },
    enabled=true 
  },
  ["workshop-2199027653598518200"]={ configuration_options={  }, enabled=true } 
}