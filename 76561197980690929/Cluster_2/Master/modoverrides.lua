return {
  ["workshop-100010002"]={
    configuration_options={
      amount_of_numbers="default",
      display_mode="waving",
      dmg_only="on",
      show_decimal_points="no" 
    },
    enabled=true 
  },
  ["workshop-100010005"]={
    configuration_options={
      MOD_RESTART_ALLOW_KILL=false,
      MOD_RESTART_ALLOW_RESTART=true,
      MOD_RESTART_ALLOW_RESURRECT=true,
      MOD_RESTART_CD_KILL=3,
      MOD_RESTART_CD_RESTART=1,
      MOD_RESTART_CD_RESURRECT=1,
      MOD_RESTART_FORCE_DROP_MODE=1,
      MOD_RESTART_IGNORING_ADMIN=true,
      MOD_RESTART_TRIGGER_MODE=1,
      MOD_RESTART_WELCOME_TIPS=true,
      MOD_RESTART_WELCOME_TIPS_TIME=6 
    },
    enabled=true 
  },
  ["workshop-100010007"]={
    configuration_options={ fast_eat="On", fast_pick_build_harvest_heal="On" },
    enabled=true 
  },
  ["workshop-100010013"]={ configuration_options={  }, enabled=true },
  ["workshop-100010015"]={ configuration_options={  }, enabled=true },
  ["workshop-100010016"]={ configuration_options={  }, enabled=true },
  ["workshop-100010017"]={ configuration_options={  }, enabled=true },
  ["workshop-100010018"]={ configuration_options={ kong=0, maxdiaoluo=4, mindiaoluo=1 }, enabled=true },
  ["workshop-100010020"]={
    configuration_options={
      RUOYINXIAN=false,
      SHENGDANSHI=false,
      baoshibaolv=1,
      blue_baoshi=1,
      er_shuoming=0,
      green_baoshi=1,
      marble_suipian=1,
      orange_baoshi=1,
      purple_baoshi=1,
      red_baoshi=1,
      shitoubaolv=1,
      thulecite_xiukuang=1,
      wajuecishu=2,
      yellow_baoshi=1,
      yi_shuoming=0 
    },
    enabled=true 
  },
  ["workshop-100010022"]={
    configuration_options={ dont_give_dubloons=2, give_dubloons=1, position=1, rec_dubloon=3, rec_gold=3 },
    enabled=true 
  },
  ["workshop-100010023"]={
    configuration_options={
      FIREOPTIONS=2,
      OVERRIDEMODE=false,
      SHAREMINIMAPPROGRESS=true,
      SHOWFIREICONS=true,
      SHOWPLAYERICONS=true,
      SHOWPLAYERSOPTIONS=2 
    },
    enabled=true 
  },
  ["workshop-100010024"]={
    configuration_options={
      Craft="Normal",
      Destroyable="yees",
      FoodSpoilage=1,
      Language="En",
      Position="Center",
      Slots=80 
    },
    enabled=true 
  },
  ["workshop-100010027"]={ configuration_options={ ["Draw over FoW"]="disabled" }, enabled=true },
  ["workshop-100010033"]={ configuration_options={ speed="0.25" }, enabled=true },
  ["workshop-100010041"]={ configuration_options={ Ownership=false, Travel_Cost=32 }, enabled=true },
  ["workshop-100010059"]={ configuration_options={  }, enabled=true },
  ["workshop-100010062"]={
    configuration_options={
      divider=5,
      random_health_value=0,
      random_range=0,
      send_unknwon_prefabs=true,
      show_type=0,
      unknwon_prefabs=1,
      use_blacklist=true 
    },
    enabled=true 
  },
  ["workshop-100010063"]={ configuration_options={ MAXSTACKSIZE=99 }, enabled=true },
  ["workshop-2199027653598511234"]={ configuration_options={  }, enabled=true },
  ["workshop-2199027653598511237"]={
    configuration_options={
      broom_damage=45,
      health=100,
      hunger=90,
      hunger_multi=4.5,
      lang=0,
      magic_broom_durability=60,
      normal_broom_damage=25,
      normal_broom_durability=50,
      sanity=250,
      speed_multi=1.5,
      speed_multi1=1.1 
    },
    enabled=true 
  },
  ["workshop-2199027653598511644"]={ configuration_options={  }, enabled=true },
  ["workshop-2199027653598511647"]={
    configuration_options={
      amount_of_numbers="default",
      display_mode="waving",
      dmg_only="on",
      show_decimal_points="no" 
    },
    enabled=true 
  },
  ["workshop-2199027653598511695"]={
    configuration_options={ fran_diet="blood", fran_difficulty="normal" },
    enabled=true 
  },
  ["workshop-2199027653598511817"]={ configuration_options={  }, enabled=true },
  ["workshop-2199027653598512055"]={ configuration_options={ Resurrection="Yes" }, enabled=true },
  ["workshop-2199027653598512289"]={ configuration_options={  }, enabled=true },
  ["workshop-2199027653598512364"]={ configuration_options={  }, enabled=true },
  ["workshop-2199027653598512398"]={ configuration_options={ key2=122 }, enabled=true },
  ["workshop-2199027653598512437"]={ configuration_options={  }, enabled=true },
  ["workshop-2199027653598512452"]={
    configuration_options={ ChineseLanguage=0, e=122, q=122, r=122, w=122 },
    enabled=true 
  } 
}